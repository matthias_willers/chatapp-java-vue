package com.example.backend.service;

import com.example.backend.model.TextMessage;
import com.example.backend.repository.TextMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TextMessageService {

    @Autowired
    TextMessageRepository textMessageRepository;

    public List<TextMessage> getAllMessages() {
        return textMessageRepository.findAll();
    }
}
