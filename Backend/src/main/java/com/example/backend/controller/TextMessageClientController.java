package com.example.backend.controller;


import com.example.backend.model.TextMessage;
import com.example.backend.repository.TextMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import java.time.LocalDateTime;

@Controller
public class TextMessageClientController {

    @Autowired
    TextMessageRepository textMessageRepository;

    @MessageMapping("/send-message")
    @SendTo("/topic/text-messages")
    public TextMessage message(TextMessage message) throws Exception {
        message.setLocalDateTime(LocalDateTime.now());
        textMessageRepository.save(message);
        Thread.sleep(1000); // simulated delay
        return message;
    }
}
