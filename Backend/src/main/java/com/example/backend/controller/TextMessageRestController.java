package com.example.backend.controller;


import com.example.backend.model.TextMessage;
import com.example.backend.service.TextMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/text-messages")
public class TextMessageRestController {

    @Autowired
    TextMessageService textMessageService;

    @GetMapping(path= "/all")
    public ResponseEntity<List<TextMessage>> getTextMessages(){
        return new ResponseEntity<>(textMessageService.getAllMessages(), HttpStatus.OK);
    }
}
