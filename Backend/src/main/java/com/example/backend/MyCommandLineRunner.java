package com.example.backend;


import com.example.backend.model.TextMessage;
import com.example.backend.repository.TextMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class MyCommandLineRunner {

    @Autowired
    TextMessageRepository textMessageRepository;

    @Bean
    CommandLineRunner commandLineRunner(){
        return args -> {

            TextMessage textMessage1 =  TextMessage.builder()
                    .message("Welcome, feel free to say hello")
                    .localDateTime(LocalDateTime.now())
                    .sender("titzko")
                    .build();

            textMessageRepository.saveAll(List.of(textMessage1));
        };
    }
}
