package com.example.backend.repository;

import com.example.backend.model.TextMessage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TextMessageRepository extends JpaRepository<TextMessage, Long> {
}
