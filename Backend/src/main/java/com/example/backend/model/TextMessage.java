package com.example.backend.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class TextMessage {

    @Id
    @SequenceGenerator(
            name = "textmessage_id_sequence",
            sequenceName = "customer_id_sequence"
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "textmessage_id_sequence"
    )
    Long id;
    String message;
    String sender;
    LocalDateTime localDateTime;

    public TextMessage(String message, String sender) {
        this.message = message;
        this.sender = sender;
    }
}
