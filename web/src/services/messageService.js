
export async function getTextMessages() {
    return fetch(`/api/v1/text-messages/all`, {
        method: "GET",
        headers: {"Content-Type": "application/json"},
    }).then(((response) =>{
        return response.json();
    }));
}