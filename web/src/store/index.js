import { createStore } from 'vuex'


export default createStore({
  state () {
    return {
      user:
        { name: ''}
    }
  },
  getters: {
    user (state) {
      return state.user;
    }
  },
  mutations: {
    addUser(state, newUser) {
        state.user = newUser
    }
  }
})
